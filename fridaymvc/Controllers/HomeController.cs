﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace fridaymvc.Controllers
{
    public class HomeController : Controller
    {
        StocksEntities2 db = new StocksEntities2();

        public ActionResult Index()
        {
            var transactionList = db.vw_transaction_list.Select(R => R).ToList();
            ViewBag.transactionList = transactionList;
            return View();
        }


        public ActionResult GetSalesByCustID(int id=1)
        {
            var salesByCustIDList = db.usp_getSalesByCustomerID(id).Select(R => R).ToList();
            ViewBag.salesByCustIDList = salesByCustIDList;
            return View();
        }
        //create the customer 
        [HttpPost]
        public ActionResult CreateCustomer([Bind(Include="FirstName,LastName,Email")] Customer customer)
        {
            if(
                string.IsNullOrEmpty(customer.FirstName) ||
                string.IsNullOrEmpty(customer.LastName) ||
                string.IsNullOrEmpty(customer.Email))
            {
                ViewBag.Message = "Error: Please enter all of the values.";
            }
            else
            {
                ViewBag.Message = "A new account has been created!";
                db.Customers.Add(customer);
                db.SaveChanges();
            }

            return View(customer);
        }

        [HttpGet]
        public ActionResult CreateCustomer()
        {
            return View(new Customer());
        }

        [HttpGet]
        public ActionResult BuyStock()
        {
            ViewBag.Stocks = db.Stocks.ToList();
            ViewBag.Customers = db.Customers.ToList();
            return View(new Sale());
        }
        [HttpPost]
        public ActionResult BuyStock([Bind(Include = "StockId,CustomerId,Quantity")] Sale sale)
        {
            ViewBag.Stocks = db.Stocks.ToList();
            ViewBag.Customers = db.Customers.ToList();

            if (sale.Quantity <= 0)
            {
                ViewBag.Message = "Error: Quantity must be greater than 0";
            } else
            {
                ViewBag.Message = "You have succesfully purchased stocks";
                db.Sales.Add(sale);
                db.SaveChanges();
            }
            return View(sale);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}