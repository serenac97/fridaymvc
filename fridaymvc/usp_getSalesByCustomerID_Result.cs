//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace fridaymvc
{
    using System;
    
    public partial class usp_getSalesByCustomerID_Result
    {
        public int Quantity { get; set; }
        public decimal CurrentPrice { get; set; }
        public string StockName { get; set; }
        public string FullName { get; set; }
        public Nullable<decimal> TotalSpent { get; set; }
    }
}
