//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace fridaymvc
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sale
    {
        public int SalesID { get; set; }
        public int StockID { get; set; }
        public int CustomerID { get; set; }
        public int Quantity { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual Stock Stock { get; set; }
    }
}
